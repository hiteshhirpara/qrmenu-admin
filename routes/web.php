<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('authentication-signin');
});
Route::get('/index', function () {
    return view('index');
});

Route::get('/list-customer', function () {
    return view('list-customer');
});
Route::get('/add-customer', function () {
    return view('add-customer');
});


Route::get('/list-admin', function () {
    return view('list-admin');
});
Route::get('/add-admin', function () {
    return view('add-admin');
});

Route::get('/reports', function () {
    return view('reports');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

/*Components*/

/*Authentication*/
Route::get('/authentication-signin', function () {
    return view('authentication-signin');
});
Route::get('/forgot-password', function () {
    return view('forgot-password');
});
