  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->	  
	  <div class="breadcrumbs page-breadcrumb d-flex align-items-center mb-3">
			<div class="breadcrumb-title pe-3">Reports</div>
			<div class="ps-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb mb-0 p-0">
						<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
						</li>
						<!-- <li class="breadcrumb-item active" aria-current="page">Reports</li> -->
					</ol>
				</nav>
			</div>
		</div>
      <!--end breadcrumb-->
      <div class="page-content">
        <div class="card">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-end">
              <div class="d-flex justify-content-end date-filter align-items-center mb-4">
                 <div class="d-flex flex-nowrap me-3 align-items-center datepicker-div">
                   <label class="white-space-nowrap me-2">From Date</label>
                   <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                       <input class="form-control" type="text" />
                       <span class="input-group-addon"><i class="fas fa-calendar-week"></i></span>
                   </div>
                 </div>
                 <div class="d-flex flex-nowrap align-items-center datepicker-div">
                   <label class="white-space-nowrap me-2">To Date</label>
                   <div id="datepicker1" class="input-group date" data-date-format="mm-dd-yyyy">
                       <input class="form-control" type="text" />
                       <span class="input-group-addon"><i class="fas fa-calendar-week"></i></span>
                   </div>
                 </div>
                
               </div>
            </div>
            <div class="table-responsive">
              <table id="example" class="table table-header table-border" style="width:100%">
                  <thead>
                    <tr>
                      
                      <th>Customer</th>
                      <th>Total Scan</th>
                      <th>Last Scanned On /date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Jacob </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Thornton </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Brielle </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Rhona </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Jacob </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Thornton </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Brielle </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                    <tr>
                      <td>Rhona </td>
                      <td>10</td>
                      <td>01/01/2021</td>
                    </tr>
                  </tbody>
                </table>
              </div>
                                            
           
          </div>
        </div>
        
      </div>
    </div>

    
    <!--end page wrapper -->
    @endsection
  
  @section("script")
 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  <script type="text/javascript">
         

          $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });

          $(function () {
            $("#datepicker1").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });


      </script>
  @endsection