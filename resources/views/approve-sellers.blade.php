  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->
       <div class="breadcrumbs d-flex align-items-center">
        <h6 class="blue-text semibold-text mb-0">Sellers Management</h6> <span class="ps-2 pe-2 gray-text"><i class="fas fa-chevron-right small"></i></span> <h6 class="blue-text semibold-text mb-0">Approve Sellers List</h6>
      </div>
      <!--end breadcrumb-->
      <div class="page-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Approve Sellers List</p>
              </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th style="width: 8px;"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th class="text-right pr-5"><span class="mr-4">Action</span></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Tiger Nixon</td>
                    <td>tigernixon@gmail.com</td>
                    <td>Texas</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Garrett Winters</td>
                    <td>garrettwinters@gmail.com</td>
                    <td>Vegas</td>
                    <td><span class="text-danger ms-2 d-block"><span class="dot position-relative"></span>Decline</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Ashton Cox</td>
                    <td>ashtoncox@gmail.com</td>
                    <td>California</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Cedric Kelly</td>
                    <td>cedrickelly@gmail.com</td>
                    <td>Newyork</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Airi Satou</td>
                    <td>airisatou@gmail.com</td>
                    <td>New Jersey</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Brielle Williamson</td>
                    <td>briellewilliamson@gmail.com</td>
                    <td>Newyork</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Herrod Chandler</td>
                    <td>herroadchandler@gmail.com</td>
                    <td>Newyork</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Rhona Davidson</td>
                    <td>RhonaDavidson@gmail.com</td>
                    <td>Newyork</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Colleen Hurst</td>
                    <td>colleenhurst@gmail.com</td>
                    <td>Texas</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Sonya Frost</td>
                    <td>sonyafrost@gmail.com</td>
                    <td>Newyork</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Approve</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-sellers" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                       
                      </ul>
                    </td>
                    
                  </tr>
                  
                </tbody>
              </table>
               <ul class="list-unstyled d-flex">
                <li><a href="#" class="btn btn-danger btn-sm me-2">Delete   </a></li>
                <li><a href="#" class="btn btn-success btn-sm me-2">Approve</a></li>
                <li><a href="#" class="btn btn-secondary btn-sm">Decline</a></li>
              </ul>
            </div>
          </div>
        </div>
        
      </div>
    </div>

    
    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection