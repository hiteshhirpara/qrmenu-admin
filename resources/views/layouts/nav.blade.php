<div class="sidebar-wrapper" data-simplebar="true">
            <div class="sidebar-header">
                <div>
                    <img src="assets/images/hamburger-menu.png" class="img-fluid menubar-icon" width="20" alt="logo icon">
                </div>
                <div>
                    <h4 class="logo-text ms-0"><a href="{{ url('dashboard') }}"><img src="assets/images/logo.png" class="logo-icon" alt="logo icon"></a></h4>
                </div>
                <div class="toggle-icon ms-auto"><i class='bx bx-menu-alt-left'></i></div>
            </div>
            <!--navigation-->
            <ul class="metismenu p-0 pt-4" id="menu">
                <li>
                    <a href="{{ url('dashboard') }}" class="">
                        <div class="parent-icon"><i class="fas fa-tachometer-alt"></i></div>
                        <div class="menu-title">Dashboard </div>
                    </a>
                </li>

                <li>
                    <a href="#" class="">
                        <div class="parent-icon"><i class="fas fa-list"></i></div>
                        <div class="menu-title">My Menu </div>
                    </a>
                </li>

                <li>
                    <a href="#" class="">
                        <div class="parent-icon"><i class="fas fa-qrcode"></i></div>
                        <div class="menu-title">QR Code</div>
                    </a>
                </li>

                <li>
                    <a href="javascript:;" class="has-arrow">
                        <div class="parent-icon"><i class="fas fa-users"></i>
                        </div>
                        <div class="menu-title">Customer Management</div>
                    </a>
                    <ul>
                        <li> <a href="{{ url('list-customer') }}"><i class="bx bx-right-arrow-alt"></i>List Customer</a>
                        </li>
                        <li> <a href="{{ url('add-customer') }}"><i class="bx bx-right-arrow-alt"></i>Add Customer</a>
                        </li>
                        
                    </ul>
                </li>

                <li>
                    <a href="#" class="">
                        <div class="parent-icon"><i class="fas fa-copyright"></i></div>
                        <div class="menu-title">My Brands</div>
                    </a>
                </li>

                <li>
                    <a href="javascript:;" class="has-arrow">
                        <div class="parent-icon"><i class="fas fa-user-cog"></i>
                        </div>
                        <div class="menu-title">Admin Management </div>
                    </a>
                    <ul>
                        <li> <a href="{{ url('list-admin') }}"><i class="bx bx-right-arrow-alt"></i>List Admin</a>
                        </li>
                        <li> <a href="{{ url('add-admin') }}"><i class="bx bx-right-arrow-alt"></i>Add Admin</a>
                        </li>
                    </ul>
                </li>
              
                <li>
                    <a href="reports">
                        <div class="parent-icon"><i class="fas fa-file-alt"></i>
                        </div>
                        <div class="menu-title">Reports</div>
                    </a>
                    
                </li>
            </ul>
            <!--end navigation-->
        </div>