    <!--start header wrapper-->
    <header>
            <div class="topbar d-flex align-items-center">
                <nav class="navbar navbar-expand">
                    <div class="mobile-toggle-menu"><i class='bx bx-menu'></i>
                    </div>
                    
                   <div class="top-menu ms-auto">
                      <span class="top-search" href="#">
                        <i class='bx bx-search'></i>
                        <input type="text" class="form-control" placeholder="Search Here">
                      </span>
                    </div>
                    <div class="user-box dropdown border-0 ms-auto ms-0">
                        <a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret p-0" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                           <!--  <img src="assets/images/avatars/avatar-2.png" class="user-img" alt="user avatar"> -->
                           <div class="user-info ps-3 pe-2">
                               <p class="user-name mb-0">Hi, Pauline</p>
                           </div>
                            <div class="user-name-box">
                                <p class="mb-0">PS</p>
                            </div>
                            
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="{{ url('authentication-signin') }}"><i class='bx bx-log-out-circle'></i><span>Logout</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
    <!-- Page wrapper end -->
