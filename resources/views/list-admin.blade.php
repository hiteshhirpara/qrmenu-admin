  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->	  
	  <div class="breadcrumbs page-breadcrumb d-flex align-items-center mb-3">
			<div class="breadcrumb-title pe-3">Admin Management</div>
			<div class="ps-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb mb-0 p-0">
						<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Admin List</li>
					</ol>
				</nav>
			</div>
		</div>
      <!--end breadcrumb-->
      <div class="page-content list-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Admin List</p>
              </div>
          </div>
          <div class="card-body position-relative">
          <div class="filter d-flex align-items-center">
            <div class="d-flex align-items-center">
              <label class="me-2">Status</label>
              <select class="form-select select-box" aria-label="Default select example">
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
              </select>
            </div>
            <div class="d-flex align-items-center ms-3">
              <label class="me-2">Search</label>
              <div class="input-group mt-3 mb-3">
                <div class="input-group-prepend">
                  <select class="form-select select-box" aria-label="Default select example">
                      <option value="1">First Name</option>
                      <option value="2">Last Name</option>
                      <option value="2">Email</option>
                  </select>
                </div>
                <input type="text" class="form-control" placeholder="Seach Here">
              </div>
              <a href="#" class="btn btn-primary btn-sm ms-2"><i class="fas fa-search"></i></a>
            </div>
          </div>
            <div class="table-responsive">
              <table id="example" class="table table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th style="width: 8px;"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th class="text-right pr-5"><span class="mr-4">Action</span></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Tiger </td>
                    <td>Nixon</td>
                    <td>tigernixon@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                   <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Garrett</td>
                    <td>Winters</td>
                    <td>garrettwinters@gmail.com</td>
                    <td><span class="text-danger ms-2 d-block"><span class="dot position-relative"></span>Inactive</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Ashton</td>
                    <td>Cox</td>
                    <td>ashtoncox@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                   <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Cedric </td>
                    <td>Kelly</td>
                    <td>cedrickelly@gmail.com</td>
                    <td><span class="text-danger ms-2 d-block"><span class="dot position-relative"></span>Inactive</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Brielle  </td>
                    <td>Williamson</td>
                    <td>briellewilliamson@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Colleen</td>
                    <td>Hurst</td>
                    <td>colleenhurst@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Rhona </td>
                    <td>Davidson</td>
                    <td>RhonaDavidson@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                   <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>Herrod  </td>
                    <td>Chandler</td>
                    <td>herroadchandler@gmail.com</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-admin" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                        
                      </ul>
                    </td>
                  </tr>
                  
                </tbody>
              </table>
              <ul class="list-unstyled d-flex">
                 <li><a href="#" class="btn btn-danger btn-sm me-2">Delete</a></li>
                <li><a href="#" class="btn btn-success btn-sm me-2">Active</a></li>
                <li><a href="#" class="btn btn-secondary btn-sm">Inactive</a></li>
               
              </ul>
            </div>
          </div>
        </div>
        
      </div>
    </div>

    
    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection