  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->
      <div class="breadcrumbs d-flex align-items-center">
        <h6 class="blue-text semibold-text mb-0">Admin Type Management</h6> <span class="ps-2 pe-2 gray-text"><i class="fas fa-chevron-right small"></i></span> <h6 class="blue-text semibold-text mb-0">Create Admin Type</h6>
      </div>
      <!--end breadcrumb-->
      <div class="page-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Create Admin Type</p>
              </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <label class="mb-2 mt-3">Name <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Display Name <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-12">
                  <label class="mb-2 mt-3">Permissions <span>*</span></label>
                  <div class="mb-3 mt-2">
                    <label><input type="checkbox" name="" value=""> Check/Uncheck All</label>
                  </div>
                  <table width="100%" class="table table-bordered">
                    <thead>
                      <th>Section</th>
                      <th width="20%" class="text-center">List</th>
                      <th width="20%" class="text-center">Add</th>
                      <th width="20%" class="text-center">Edit</th>
                      <th width="20%" class="text-center">Delete</th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Buyers</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Sellers</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Products</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Categories</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Orders</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Collections</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                      <tr>
                        <td>Admin</td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                        <td class="text-center"><input type="checkbox" name="" value=""></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
              <div class="col-md-12">
                <ul class="list-unstyled d-flex justify-content-center mt-5">
                  <li><a href="#" class="btn btn-warning  btn-md me-2">Save & Add New</a></li>
                  <li><a href="#" class="btn btn-success btn-md me-2">Save</a></li>
                  <li><a href="#" class="btn blue-btn  btn-md me-2">Save & Close</a></li>
                  <li><a href="#" class="btn btn-secondary btn-md">Cancel</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>

  

    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection