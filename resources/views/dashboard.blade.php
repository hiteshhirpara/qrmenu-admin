@extends("layouts.app")
@section("style")
    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
@endsection

@section("wrapper")
    <div class="page-wrapper">
            <div class="page-content">
                <h1 class="font-30 mb-4">Dashboard</h1>
                <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                    <div class="col">
                        <div class="card radius-10 overflow-hidden">
                            <div class="card-body ps-4">
                                <div class="dashbox-line bg-success"></div>
                                <div class="d-flex align-items-center">
                                    <div>
                                        <p class="mb-0 text-secondary">Total Revenue</p>
                                        <h4 class="my-1 font-weight-700">$12,500</h4>
                                    </div>
                                    <div class="widgets-icons bg-light-success text-success ms-auto"><i class='bx bxs-wallet'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card radius-10 overflow-hidden">
                            <div class="card-body ps-4">
                                <div class="dashbox-line bg-warning"></div>    
                                <div class="d-flex align-items-center">
                                    <div>
                                        <p class="mb-0 text-secondary">Total Customers</p>
                                        <h4 class="my-1 font-weight-700">8.4K</h4>
                                    </div>
                                    <div class="widgets-icons bg-light-warning text-warning ms-auto"><i class='bx bxs-group'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card radius-10 overflow-hidden">
                            <div class="card-body ps-4">
                                <div class="dashbox-line bg-danger"></div>
                                <div class="d-flex align-items-center">
                                    <div>
                                        <p class="mb-0 text-secondary">Store Visitors</p>
                                        <h4 class="my-1 font-weight-700">59K</h4>
                                    </div>
                                    <div class="widgets-icons bg-light-danger text-danger ms-auto"><i class='bx bxs-binoculars'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
                <div class="row">
                    <div class="col-md-7 d-flex">
                        <div class="card radius-10 w-100">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h5 class="mb-1 font-weight-700">Store Metrics</h5>
                                    </div>
                                    <!-- <div class="font-22 ms-auto">
                                        <i class='bx bx-dots-horizontal-rounded'></i>
                                    </div> -->
                                </div>
                                <div id="chart4">
                                    <img src="assets/images/dash-chart-1.png" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 d-flex">
                        <div class="card radius-10 w-100">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h5 class="mb-1 font-weight-700">Top Products</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="product-list p-3 mb-0 pt-0"> <!-- data-simplebar -->
                                <div class="row border mx-0 mb-3 py-2 radius-10 cursor-pointer align-items-center">
                                    <div class="col-sm-8">
                                        <div class="d-flex align-items-center">
                                            <div class="product-img">
                                                <img src="assets/images/icons/chair.png" alt="" />
                                            </div>
                                            <div class="ms-2">
                                                <h6 class="mb-1">Light Blue Chair</h6>
                                                <p class="mb-0">Lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right ms-auto col">
                                        <h6 class="mb-1"><strong>$156</strong></h6>
                                    </div>
                                </div>
                                <div class="row border mx-0 mb-3 py-2 radius-10 cursor-pointer align-items-center">
                                    <div class="col-sm-8">
                                        <div class="d-flex align-items-center">
                                            <div class="product-img">
                                                <img src="assets/images/icons/chair.png" alt="" />
                                            </div>
                                            <div class="ms-2">
                                                <h6 class="mb-1">Light Blue Chair</h6>
                                                <p class="mb-0">Lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right ms-auto col">
                                        <h6 class="mb-1"><strong>$156</strong></h6>
                                    </div>
                                </div>
                                <div class="row border mx-0 mb-3 py-2 radius-10 cursor-pointer align-items-center">
                                    <div class="col-sm-8">
                                        <div class="d-flex align-items-center">
                                            <div class="product-img">
                                                <img src="assets/images/icons/chair.png" alt="" />
                                            </div>
                                            <div class="ms-2">
                                                <h6 class="mb-1">Light Blue Chair</h6>
                                                <p class="mb-0">Lorem ipsum</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right ms-auto col">
                                        <h6 class="mb-1"><strong>$156</strong></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-md-8 d-flex">
                        <div class="card radius-10 w-100">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h5 class="mb-1 font-weight-700">New Customers</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="customers-list p-3 mb-3 pt-0">
                                <div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
                                    <div class="">
                                        <img src="assets/images/avatars/avatar-3.png" class="rounded-circle" width="46" height="46" alt="" />
                                    </div>
                                    <div class="ms-2">
                                        <h6 class="mb-1 ">Emy Jackson</h6>
                                        <p class="mb-0 font-14 text-secondary">emy_jac@xyz.com</p>
                                    </div>
                                    <div class="list-inline d-flex customers-contacts ms-auto"> 
                                        <a href="javascript:;" class="list-inline-item"><i class='bx bx-dots-vertical-rounded'></i></a>
                                    </div>
                                </div>
                                <div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
                                    <div class="">
                                        <img src="assets/images/avatars/avatar-3.png" class="rounded-circle" width="46" height="46" alt="" />
                                    </div>
                                    <div class="ms-2">
                                        <h6 class="mb-1 ">Emy Jackson</h6>
                                        <p class="mb-0 font-14 text-secondary">emy_jac@xyz.com</p>
                                    </div>
                                    <div class="list-inline d-flex customers-contacts ms-auto"> 
                                        <a href="javascript:;" class="list-inline-item"><i class='bx bx-dots-vertical-rounded'></i></a>
                                    </div>
                                </div>
                                <div class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer">
                                    <div class="">
                                        <img src="assets/images/avatars/avatar-3.png" class="rounded-circle" width="46" height="46" alt="" />
                                    </div>
                                    <div class="ms-2">
                                        <h6 class="mb-1 ">Emy Jackson</h6>
                                        <p class="mb-0 font-14 text-secondary">emy_jac@xyz.com</p>
                                    </div>
                                    <div class="list-inline d-flex customers-contacts ms-auto"> 
                                        <a href="javascript:;" class="list-inline-item"><i class='bx bx-dots-vertical-rounded'></i></a>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex">
                        <div class="card radius-10 w-100">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h5 class="mb-1 font-weight-700">Product Status</h5>
                                    </div>
                                    <!-- <div class="font-22 ms-auto">
                                        <i class='bx bx-dots-horizontal-rounded'></i>
                                    </div> -->
                                </div>
                                <div id="chart4">
                                    <img src="assets/images/dash-chart-2.png" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
                
               
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h5 class="mb-0 font-weight-700">Orders Summary</h5>
                            </div>
                            <div class="font-22 ms-auto"><i class='bx bx-dots-horizontal-rounded'></i>
                            </div>
                        </div>
                        <hr/>
                        <div class="table-responsive">
                            <table class="table align-middle mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th>Order id</th>
                                        <th>Product</th>
                                        <th>Customer</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#897656</td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="recent-product-img">
                                                    <img src="assets/images/icons/chair.png" alt="">
                                                </div>
                                                <div class="ms-2">
                                                    <h6 class="mb-1">Light Blue Chair</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Brooklyn Zeo</td>
                                        <td>12 Jul 2020</td>
                                        <td>$64.00</td>
                                        <td>
                                            <div class="d-flex align-items-center text-danger"> <i class='bx bx-radio-circle-marked bx-burst bx-rotate-90 align-middle font-18 me-1'></i>
                                                <span>Pending</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex order-actions">  
                                                <a href="javascript:;" class=""><i class="bx bx-cog"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#987549</td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="recent-product-img">
                                                    <img src="assets/images/icons/shoes.png" alt="">
                                                </div>
                                                <div class="ms-2">
                                                    <h6 class="mb-1">Green Sport Shoes</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Martin Hughes</td>
                                        <td>14 Jul 2020</td>
                                        <td>$45.00</td>
                                        <td>
                                            <div class="d-flex align-items-center text-primary">    <i class='bx bx-radio-circle-marked bx-burst bx-rotate-90 align-middle font-18 me-1'></i>
                                                <span>Dispatched</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex order-actions">  
                                                <a href="javascript:;" class=""><i class="bx bx-cog"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#685749</td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="recent-product-img">
                                                    <img src="assets/images/icons/headphones.png" alt="">
                                                </div>
                                                <div class="ms-2">
                                                    <h6 class="mb-1">Red Headphone 07</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Shoan Stephen</td>
                                        <td>15 Jul 2020</td>
                                        <td>$67.00</td>
                                        <td>
                                            <div class="d-flex align-items-center text-success">    <i class='bx bx-radio-circle-marked bx-burst bx-rotate-90 align-middle font-18 me-1'></i>
                                                <span>Completed</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex order-actions">  
                                                <a href="javascript:;" class=""><i class="bx bx-cog"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>#887459</td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="recent-product-img">
                                                    <img src="assets/images/icons/idea.png" alt="">
                                                </div>
                                                <div class="ms-2">
                                                    <h6 class="mb-1">Mini Laptop Device</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Alister Campel</td>
                                        <td>18 Jul 2020</td>
                                        <td>$87.00</td>
                                        <td>
                                            <div class="d-flex align-items-center text-success">    <i class='bx bx-radio-circle-marked bx-burst bx-rotate-90 align-middle font-18 me-1'></i>
                                                <span>Completed</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex order-actions">  
                                                <a href="javascript:;" class=""><i class="bx bx-cog"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section("script")
    <script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
    <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
    <script src="assets/js/index.js"></script>
@endsection
