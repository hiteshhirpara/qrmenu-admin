  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->
      <div class="breadcrumbs d-flex align-items-center">
        <h6 class="blue-text semibold-text mb-0">Change Password</h6>
      </div>
      <!--end breadcrumb-->
      <div class="page-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Change Password</p>
              </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <label class="mb-2 mt-3">Old Password</label>
                <input type="text" name="password" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">New Password</label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                <label class="mb-2 mt-3">Confirm Password</label>
                <input type="text" name="name" class="form-control">
              </div>
              
              </div>
              <div class="col-md-12">
                <ul class="list-unstyled d-flex justify-content-center mt-5 flex-wrap">
                  <li><a href="#" class="btn btn-primary btn-md me-2 mb-2">Save</a></li>
                  <li><a href="#" class="btn btn-secondary btn-md mb-2">Cancel</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>

  

    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection