<!doctype html>
<html lang="en">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--favicon-->
		<link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
		<!-- loader-->
		<link href="assets/css/pace.min.css" rel="stylesheet" />
		<script src="assets/js/pace.min.js"></script>
		<!-- Bootstrap CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
		<link href="assets/css/app.css" rel="stylesheet">
		<link href="assets/css/icons.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/custom.css" />
		<title>Forgot Password | QR Menu</title>
	</head>

<body>
	<!--wrapper-->
    <section class="main">
            <div class="left-side text-center">
                 <h1 class="white-text">Welcome to <br> <strong>QR Menu</strong></h1>
            </div>
            <div class="right-side">
                <a href="#" class="mb-5 d-block"><img src="assets/images/logo.png" width="230" class="img-fluid d-block ms-auto me-auto"></a>
                <h3><strong>Forgotten Password?</strong></h3>
                <p>Enter your email to reset your password</p>
                <form class="pt-2">
                    <div class="mb-3">
                        <input type="text" name="email" placeholder="Email" class="form-control">
                    </div>
                    
                    <div class="text-center pt-2">
                        <a href="list-admin" class="btn btn-primary">SUBMIT</a>
                    </div>
                </form>
                
            </div>
        </section>
	<!--end wrapper-->

	<!--plugins-->
	<script src="assets/js/jquery.min.js"></script>

</body>

</html>
