  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->
       <!-- <div class=" d-flex align-items-center">
        <h6 class="blue-text semibold-text mb-0">Customer Management</h6> <span class="ps-2 pe-2 gray-text"><i class="fas fa-chevron-right small"></i></span> <h6 class="blue-text semibold-text mb-0">Customer List</h6>
      </div> -->
	  
		<div class="breadcrumbs page-breadcrumb d-flex align-items-center mb-3">
			<div class="breadcrumb-title pe-3">Customer Management</div>
			<div class="ps-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb mb-0 p-0">
						<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Customer List</li>
					</ol>
				</nav>
			</div>
		</div>
      <!--end breadcrumb-->
      <div class="page-content list-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Customer List</p>
              </div>
          </div>
          <div class="card-body  position-relative">
            <div class="filter d-flex align-items-center">
             
              <div class="d-flex align-items-center">
                <label class="me-2">Status</label>
                <select class="form-select select-box" aria-label="Default select example">
                    <option value="1">Active</option>
                    <option value="2">Inactive</option>
                </select>
              </div>
              <div class="d-flex align-items-center ms-3">
                <label class="me-2">Search</label>
                <div class="input-group mt-3 mb-3">
                  <div class="input-group-prepend">
                    <select class="form-select select-box" aria-label="Default select example">
                        <option value="1">Name</option>
                        <option value="2">Business Name</option>
                    </select>
                  </div>
                  <input type="text" class="form-control" placeholder="Seach Here">
                </div>
                <a href="#" class="btn btn-primary btn-sm ms-2"><i class="fas fa-search"></i></a>
              </div>
            </div>
            <div class="table-responsive">
              <table id="example" class="table table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th style="width: 8px;"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Business Name</th>
                    <th>Date of join</th>
                    <th>No of Menus</th>
                    <th>Status</th>
                    <th class="text-right pr-5"><span class="mr-4">Action</span></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>100</td>
                    <td>Tiger</td>
                    <td>Nixon</td>
                    <td>20/01/2018</td>
                    <td>5</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>101</td>
                    <td>John</td>
                    <td>The Zebra</td>
                    <td>18/01/2019</td>
                    <td>10</td>
                    <td><span class="text-danger ms-2 d-block"><span class="dot position-relative"></span>Inctive</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                   <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>102</td>
                    <td>Shrof</td>
                    <td>Arctic</td>
                    <td>17/01/2018</td>
                    <td>5</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>100</td>
                    <td>Tiger</td>
                    <td>Nixon</td>
                    <td>20/01/2018</td>
                    <td>5</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>101</td>
                    <td>John</td>
                    <td>The Zebra</td>
                    <td>18/01/2019</td>
                    <td>10</td>
                    <td><span class="text-danger ms-2 d-block"><span class="dot position-relative"></span>Inctive</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                   <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>102</td>
                    <td>Shrof</td>
                    <td>Arctic</td>
                    <td>17/01/2018</td>
                    <td>5</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="" value=""></td>
                    <td>100</td>
                    <td>Tiger</td>
                    <td>Nixon</td>
                    <td>20/01/2018</td>
                    <td>5</td>
                    <td><span class="text-success ms-2 d-block"><span class="dot position-relative"></span>Active</span></td>
                    <td>
                      <ul class="list-unstyled list-inline mb-0 float-end">
                        <li class="list-inline-item"><a href="add-customer" class="btn btn-edit"><img src="assets/images/edit.png" class="img-fluid" width="12" height="12"></a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-trash"><img src="assets/images/trash.png" class="img-fluid" width="12" height="12"></a></li>
                      </ul>
                    </td>
                    
                  </tr>
                </tbody>
              </table>
               <ul class="list-unstyled d-flex">
                <li><a href="#" class="btn btn-danger btn-sm me-2">Delete</a></li>
                <li><a href="#" class="btn btn-success btn-sm me-2">Active</a></li>
                <li><a href="#" class="btn btn-secondary btn-sm">Inactive</a></li>
              </ul>
            </div>
          </div>
        </div>
        
      </div>
    </div>

    
    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection