  @extends("layouts.app")

  @section("style")
  <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
  @endsection

    @section("wrapper")
    <!--start page wrapper -->
    <div class="page-wrapper">
      <!--breadcrumb-->
	  <div class="breadcrumbs page-breadcrumb d-flex align-items-center mb-3">
			<div class="breadcrumb-title pe-3">Admin Management</div>
			<div class="ps-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb mb-0 p-0">
						<li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Add / Edit Admin</li>
					</ol>
				</nav>
			</div>
		</div>
      <!--end breadcrumb-->
      <div class="page-content">
        <div class="card">
          <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 fw-bold">Add / Edit Admin</p>
              </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <label class="mb-2 mt-3">First Name <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Last Name <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                <label class="mb-2 mt-3">Email <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Password <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Confirm Password <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Admin Type <span>*</span></label>
                <input type="text" name="name" class="form-control">
              </div>
             
              <div class="col-md-6">
                 <label class="mb-2 mt-3">Status <span>*</span></label>
                <select class="form-select select-box form-control" aria-label="Default select example">
                    <option selected="">Select Options</option>
                    <option value="1">Active</option>
                    <option value="2">Inactive</option>
                </select>
              </div>
              <div class="col-md-12">
                <ul class="list-unstyled d-flex justify-content-center mt-5 flex-wrap">
                  <li><a href="#" class="btn btn-primary btn-md me-2 mb-2">Save</a></li>
                  <li><a href="#" class="btn btn-secondary btn-md mb-2">Cancel</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>

  

    <!--end page wrapper -->
    @endsection
  
  @section("script")
  <script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
      } );
  </script>
  <script>
    $(document).ready(function() {
      var table = $('#example2').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print']
      } );
     
      table.buttons().container()
        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
    } );
  </script>
  @endsection